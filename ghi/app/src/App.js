import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManuList from "./inventory/ManuList";
import ManuForm from "./inventory/ManuForm";
import ModelList from "./inventory/ModelList";
import ModelForm from "./inventory/ModelForm";
import SalesPeopleList from "./sales/SalePeopleList";
import SalesPeopleForm from "./sales/SalePeopleForm";
import CustomerList from "./sales/CustomerList";
import CustomerForm from "./sales/CustomerForm";
import SalesForm from "./sales/SalesForm";
import SalesList from "./sales/SalesList";
import SalesHistory from "./sales/SalesHistory";
import AutomobileList from "./inventory/AutomobileList";
import AutomobileForm from "./inventory/AutomobileForm";
import TechnicianList from "./services/TechnicianList";
import TechnicianForm from "./services/TechnicianForm";
import AppointmentList from "./services/AppointmentList";
import AppointmentForm from "./services/AppointmentForm";
import AppointmentHistory from "./services/ServiceHistory";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="Manufacturers">
            <Route path="" element={<ManuList />} />
            <Route path="new" element={<ManuForm />} />
          </Route>
          <Route path="Models">
            <Route path="" element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path='Automobiles'>
            <Route path='' element ={< AutomobileList />} />
            <Route path='new' element ={< AutomobileForm /> } />
          </Route>
          <Route path='Technicians'>
            <Route path='' element ={< TechnicianList />} />
            <Route path='new' element ={< TechnicianForm /> } />
          </Route>
          <Route path='Appointment'>
            <Route path='' element ={< AppointmentList />} />
            <Route path='new' element ={< AppointmentForm /> } />
          </Route>
          <Route path ='Service_history'>
            <Route path ='' element={< AppointmentHistory />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalesPeopleList />} />
            <Route path="new" element={<SalesPeopleForm />} />
          </Route>
          <Route path="customer">
            <Route path="" element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
