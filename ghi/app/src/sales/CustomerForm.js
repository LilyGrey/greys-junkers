import React, { useState } from "react";

function CustomerForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };
  const handleAddressChange = (e) => {
    const value = e.target.value;
    setAddress(value);
  };
  const handlePhoneNumberChange = (e) => {
    const value = e.target.value;
    setPhoneNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      address: address,
      phone_number: phoneNumber,
    };

    const Url = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(Url, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setAddress("");
      setPhoneNumber("");
    }
  };
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input
                  value={firstName}
                  onChange={handleFirstNameChange}
                  placeholder="first_name"
                  required
                  type="text"
                  name="first_name"
                  id="first_name"
                  className="form-control"
                />
                <label htmlFor="name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={lastName}
                  onChange={handleLastNameChange}
                  placeholder="last_name"
                  required
                  type="text"
                  name="last_name"
                  id="last_name"
                  className="form-control"
                />
                <label htmlFor="name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={address}
                  onChange={handleAddressChange}
                  placeholder="address"
                  required
                  type="text"
                  name="address"
                  id="address"
                  className="form-control"
                />
                <label htmlFor="name">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={phoneNumber}
                  onChange={handlePhoneNumberChange}
                  placeholder="phone_number"
                  required
                  type="text"
                  name="phone_number"
                  id="phone_number"
                  className="form-control"
                />
                <label htmlFor="name">Phone Number</label>
              </div>
              <div>
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
