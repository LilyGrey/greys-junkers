import { useEffect, useState } from "react";

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalePeople] = useState([]);
  const [salesperson, setSalesPerson] = useState("");

  const handleSalesPersonChange = (e) => {
    const value = e.target.value;
    setSalesPerson(value);
  };

  const getSale = async () => {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };

  const getSalePeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalePeople(data.salespeople);
    }
  };

  useEffect(() => {
    getSale();
    getSalePeople();
  }, []);

  return (
    <div>
      <h1> Sales </h1>
      <div className="form-floating mb-3">
        <select
          value={salesperson}
          required
          onChange={handleSalesPersonChange}
          name="salesperson"
          id="salesperson"
          className="form-select"
        >
          <option value="">Select SalesPerson</option>
          {salespeople.map((salesperson) => {
            return (
              <option
                key={salesperson.employee_id}
                value={salesperson.employee_id}
              >
                {salesperson.first_name} {salesperson.last_name}
              </option>
            );
          })}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.filter((sale) => sale.salesperson.employee_id === salesperson).map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>
                    {sale.salesperson.first_name} {sale.salesperson.last_name}
                  </td>
                  <td>
                    {sale.customer.first_name} {sale.customer.last_name}
                  </td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory;
