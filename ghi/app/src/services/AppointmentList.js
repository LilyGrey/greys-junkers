import React, { useEffect, useState } from "react";

function AppointmentList(props) {
    const[appointments, setAppointments]= useState([]);
    const[autos, setAutomobiles]= useState([]);

    const loadAppointments=async () =>{
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        } else {
          console.error(response);
        }
      }

      useEffect(() => {
        loadAppointments();
      }, []);

      const loadAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();
          setAutomobiles(data.autos);
        } else {
          console.error(response);
        }
      }

      useEffect(() => {
        loadAutomobiles();
      }, []);

      const handleCancel= async (id)=>{
        const url =`http://localhost:8080/api/appointments/${id}/cancel/`;

        const stat =appointments.find(row=>row.id =id)
        stat.status = 'Canceled'

        const fetchConfig={
          method: 'PUT',
          body: JSON.stringify(stat),
          headers: {
            'Content-Type': 'application/json',
          }
        }
        const response =await fetch(url, fetchConfig);
        if (response.ok) {
          loadAppointments();
        }
      };

      const handleFinish= async (id)=>{
        const url =`http://localhost:8080/api/appointments/${id}/finish/`;

        const done =appointments.find(row=>row.id =id)
        done.status = 'Finished'

        const fetchConfig={
          method: 'PUT',
          body: JSON.stringify(done),
          headers: {
            'Content-Type': 'application/json',
          }
        }
        const response =await fetch(url, fetchConfig);
        if (response.ok) {
          loadAppointments();
        }
      };
      const filtered = appointments.filter((appointment)=> appointment.status !== 'Finished' && appointment.status !== "Canceled")

      return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date / Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
              <th>Update Status</th>
            </tr>
          </thead>
          <tbody>
            {filtered?.map(appointment => {
              const automobile =autos.find(auto=>auto.vin===appointment.vin);
              return (
                <tr key={appointment.vin}>
                  <td>{ appointment.vin }</td>
                  <td>{ automobile ? automobile.sold ? "Yes" : 'No' :"No"}</td>
                  <td>{ appointment.customer }</td>
                  <td>{ appointment.date_time }</td>
                  <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.status ? "Created" : "Created"}</td>
                  <td>
                        <button type="button" className="btn btn-danger" onClick={()=>handleCancel(appointment.id)}>Cancel</button>
                        <button type="button" className="btn btn-success" onClick={()=>handleFinish(appointment.id)}>Finish</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }

    export default AppointmentList;
