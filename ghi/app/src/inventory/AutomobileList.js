import { useEffect, useState } from "react";

function AutomobileList () {
    const[automobiles, setAutomboile]=useState([])

    const getData = async () => {
        const response = await fetch ('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomboile(data.autos)
        }
    }
    useEffect(()=> {
        getData()
    }, [])
    return(
        <div>
          <table className = 'table table-striped table-hover'>
            <thead>
              <tr>
                <th>Model</th>
                <th>Year</th>
                <th>Color</th>
                <th>VIN</th>
                <th>Manufacturer</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {automobiles.map( auto => {
                return (
                  <tr key ={auto.id}>
                    <td>{ auto.model.name }</td>
                    <td>{ auto.year }</td>
                    <td>{ auto.color }</td>
                    <td>{ auto.vin }</td>
                    <td>{ auto.model.manufacturer.name }</td>
                    <td> { auto.sold ? "Yes" : "No"} </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          </div>

          );
    }

    export default AutomobileList
