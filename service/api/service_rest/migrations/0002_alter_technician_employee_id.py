# Generated by Django 4.0.3 on 2023-09-06 01:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.PositiveSmallIntegerField(auto_created=True),
        ),
    ]
