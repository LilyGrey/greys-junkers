# CarCar

Team:

* Ben Litvack- Service
* Lily Grey - Sales

## Design
![Diagram](https://media.discordapp.net/attachments/1140804304945557595/1149449288561209414/image.png?width=810&height=376)

## Service microservice

### Technician
The model that represents the technician who will be assigned to complete any service repairs.

Includes their first and last name, as well as an employee ID that is set to be unique.

### AutomobileVO
The Value object model that reprsents the automobiles pulled from the inventory API

Includes a unique VIN field, with a max length of 17, as well as a boolean field to track whether the car was sold or not

### Appointment
The model that represents all appointments for automobiles

This model includes fields for: Date/Time, reason for appointment, a unique VIN, customer name, a foreign key field for the technician performing the service, and the status of the service (whether it was 'created', 'canceled', or 'finished)

### Status
As you can see in the diagram, Status was originally its own model with a field of its name, yet it was decided to consolidate it into appointment to minimize the amount of required views and urls.  It is no longer a model in the final project.

## Sales microservice
My sales microservice has three models and one Value Object, SalesPerson, Customer, Sale and AutomobileVO. For the sales model I needed to make foreign keys for autombileVO, SalesPerson and Customer since it requires the data from those models and value objects. For the autombileVO vin I made it capped at 17 since that is the legnth of VINs and made it unique since each vin is unique to that exact car. I also did a similar thing with employee_id in salesperson and phone_number in customer to insure there is no duplicate numbers or employee ids.


## Step-by-step Instructions to run the project

1. Fork the Repo

2. Clone the project

3. Run the following codes in your terminal
   - `docker volume create beta-data`
   - `docker-compose build`
   - `docker-compose up`

### URL Endpoints and Samples
In all following images, one will be able to see the relevant URL, endpoint, HTTP method, along with both the request and response bodies.  The order for these will be done List, POST, PUT, DELETE (assuming all are available).  For the purpose of space, all list HTTP requests will only show the first in said list.
#### Service API
List all Technicians
![Alt text](image-1.png)

Add a Technician
![Alt text](image-3.png)

Delete a Technician
![Alt text](image-4.png)

List All Service Appointments
![Alt text](image.png)

Create an Appointment
![Alt text](image-5.png)

Finish/Cancel an Appointment
![Alt text](image-16.png)
![Alt text](image-17.png)

Delete an appointment
![Alt text](image-6.png)

#### Sales API

List all Sales
![Alt text](image-7.png)

Add a Sale
![Alt text](image-8.png)

Delete a Sale
![Alt text](image-9.png)

List Customers
![Alt text](image-10.png)

Add a Customer
![Alt text](image-11.png)

Delete a Customer
![Alt text](image-12.png)

List Sales Peeps
![Alt text](image-13.png)

Add Sales Peeps
![Alt text](image-14.png)

Delete Sales Peeps
![Alt text](image-15.png)
